/**********************************************
* File: AVLAuthorTest.cpp
* Author: 
* Email: 
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

// Struct goes here
struct Author{
	std::string FirstName;
	std::string LastName;

	Author(std::string FirstName, std::string LastName) : FirstName(FirstName), LastName(LastName) {}

	bool operator<(const Author& rhs) const{
		
		// lhs < rhs
		if(LastName < rhs.LastName)
			return true;
		else if (LastName == rhs.LastName){
			if(FirstName < rhs.FirstName)
				return true;
		}
		return false;
	}

	bool operator==(const Author& rhs) const{

		if (LastName != rhs.LastName)
			return false;
		else {
			if (FirstName != rhs.FirstName)
				return false;
		}
		return true;
	}

	friend std::ostream& operator<<(std::ostream& outStream, const Author& printAuth);
};

std::ostream& operator<<(std::ostream& outStream, const Author& printAuth){

	outStream << printAuth.LastName << ", " << printAuth.FirstName;
	
	return outStream;
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
	//create tree
	AVLTree<Author> authorAVL;

	Author Aardvark("Anthony", "Aardvark");
	Author Eckburg("Thomas", "Eckburg");
	Author Meow("Mr.", "Whiskers");
	Author Greg("Greg", "Aardvark");
	Author BP("Bad", "Person");

	authorAVL.insert(Aardvark);
	authorAVL.insert(Eckburg);
	authorAVL.insert(Meow);
	authorAVL.insert(Greg);
	authorAVL.insert(BP);

	
	authorAVL.printTree();

	authorAVL.remove(BP);

	std::cout << "\nRemove bad person!!!" << std::endl;
	authorAVL.printTree();
	
    return 0;
}
