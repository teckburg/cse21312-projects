/**********************************************
* File: BTreeTestData.cpp
* Author: Thomas Eckburg
* Email: teckburg@nd.edu
*  
**********************************************/
#include "BTree.h"
#include <cstdlib>

struct Data{
	int data, key;

	/********************************************
	* Function Name  : Data
	* Pre-conditions : none
	* Post-conditions: none
	* 
	* Data default constructor
	********************************************/
	Data() :
		data { 0 },
		key  { 0 } {}


	/********************************************
	* Function Name  : Data
	* Pre-conditions : int d, int k
	* Post-conditions: none
	* 
	* Data constructor with data and key specified
	********************************************/
	Data(int d, int k) :
		data { d },
		key  { k } {}

	/********************************************
	* Function Name  : operator<
	* Pre-conditions : const Data& d
	* Post-conditions: bool
	* 
	* Overloaded operator function to find which key/value pair
	* is smaller
	********************************************/
	bool operator<(const Data& d) const
	{
		return (key < d.key);
	}
	/********************************************
	* Function Name  : operator>
	* Pre-conditions : const Data &d
	* Post-conditions: bool
	* 
	* Overloaded operator function to find which key/value
	* pair is larger
	********************************************/
	bool operator>(const Data &d) const
	{
		return (key > d.key);
	}
	/********************************************
	* Function Name  : operator==
	* Pre-conditions : const Data &d
	* Post-conditions: bool
	* 
	* Overloaded operator function to find if a key/value
	* pair has equivalent keys
	********************************************/
	bool operator==(const Data &d) const
	{
		return (d.key == key);
	}
};
	/********************************************
	* Function Name  : operator<<
	* Pre-conditions : std::ostream& os, const Data& d
	* Post-conditions: std::ostream&
	* 
	* Overloaded operator function to print out a key/value
	* pair of a Data struct
	********************************************/
	std::ostream& operator<<(std::ostream& os, const Data& d)
	{
		os << "Key is: " << d.key << std::endl;
		os << "Data is: " << d.data << std::endl << std::endl;
	}


/********************************************
* Function Name  : createRand
* Pre-conditions : BTree<struct Data> *tree
* Post-conditions: none
* 
* This function generates a random Data struct and inserts it into
* the BTree passed to it.
********************************************/
void createRand(BTree<struct Data> *tree)
{
	int k = rand() % 1000;
	int d = rand() % 1000;
	Data dat = Data(d, k);
	tree->insert(dat);	
}


/********************************************
* Function Name  : findRand
* Pre-conditions : BTree<struct Data> * tree
* Post-conditions: none
* 
* This function generates a random key/data value pair and checks
* to see if it exists in the BTree passed to it. If it does exist,
* it informs the user that it found a pair and prints out the associated
* key/data pair. Otherwise, it tells the user that the key was not found.
********************************************/
void findRand(BTree<struct Data> * tree)
{
	int k = rand() % 1000;
	int d = rand() % 1000;

	Data sData = Data(d, k);
	
	std::cout << "Search for key: " << k << std::endl;
	Data * keysFound;
	
	if (tree->search(sData) != NULL)
	{
		keysFound = (tree->search(sData))->keys;
		std::cout << "Found pair with key of " << k << std::endl;
	
		for(int i = 0; i <= 2 ; i++)
		{
			if (*(keysFound+i) == sData)
				std::cout << *(keysFound+i);
		}
	}
	else
	{
		std::cout << "Pair with key of " << k << " not found.\n" << std::endl;
	}
}


/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* The main driver function of the program. Creates a BTree, inserts 1000 random
* Data struct objects into it, then searches for 10 random keys.
********************************************/
int main(int argc, char** argv)
{
	// BTree using struct Data as the tree's data values
	BTree<struct Data> t(2); // A B-Tree with minnimum degree 2
	/*
	// Test case 1: Inserting first data set
	std::cout << "Inserting (1,2), (2,3), (4,6), (1,1), (2,2), (3,2)\n" << std::endl;
	t.insert(Data(1,2));
	t.insert(Data(2,3));
	t.insert(Data(4,6));
	t.insert(Data(1,1));
	t.insert(Data(2,2));
	t.insert(Data(3,2));
	
	// Test traversal of the tree
	std::cout << "Traversal:\n";
	t.traverse();

	std::cout << "Deleting node with key of 1\n" << std::endl;
	t.remove(Data(1,1));
	std::cout << "Traversal:\n";
	t.traverse();

	std::cout << "Search for key of 2\n";
	Data sData = Data(1,2);
	Data  keysFound[3] = {*(t.search(sData))->keys};
	for(int i = 0; i < 4; i++)
	{
		if (keysFound[i] == sData)
			std::cout << "here: " << keysFound[i];//(t.search(Data(1,1)))->keys;
	}*/
	/*
	// Test case 2: Inserting second data set
	std::cout << "Inserting (50,1200), (400,19), (74200,6), (1,3), (200,300), (10000,20000)\n" << std::endl;
	t.insert(Data(50,1200));
	t.insert(Data(400, 19));
	t.insert(Data(74200, 6));
	t.insert(Data(1,3));
	t.insert(Data(200,300));
	t.insert(Data(10000, 20000));
	
	// Test traversal of the tree
	std::cout << "Traversal:\n";
	t.traverse();

	std::cout << "Deleting node with key of 300\n" << std::endl;
	t.remove(Data(200,300));
	std::cout << "Traversal:\n";
	t.traverse();

	Data sData = Data(210,6);
	std::cout << "Search for key:\n" << sData;
	Data * keysFound;
	keysFound = (t.search(sData))->keys;
	std::cout << "found?\n";// << keysFound[1];
	
	for(int i = 0; i <= 2 ; i++)
	{
		if (*(keysFound+i) == sData)
			std::cout << *(keysFound+i);//(t.search(Data(1,1)))->keys;
	}
	*/
	/*
	// Test case 3: Inserting third data set
	std::cout << "Inserting (1,1), (1,2), (10,13), (40,40), (500,500), (90,90)\n" << std::endl;
	t.insert(Data(1,1));
	t.insert(Data(1, 2));
	t.insert(Data(10, 13));
	t.insert(Data(40, 40));
	t.insert(Data(500, 50));
	t.insert(Data(90, 90));
	
	// Test traversal of the tree
	std::cout << "Traversal:\n";
	t.traverse();

	std::cout << "Deleting node with key of 13\n" << std::endl;
	t.remove(Data(10,13));
	std::cout << "Traversal:\n";
	t.traverse();

	Data sData = Data(500,50);
	std::cout << "Search for key:\n" << sData;
	Data * keysFound;
	//std::cout << "Crashed here?\n";
	if (t.search(sData) != NULL)
	{
		keysFound = (t.search(sData))->keys;
	//std::cout << "Crashed here2?\n";
	std::cout << "found?\n";// << keysFound[1];
	
	for(int i = 0; i <= 2 ; i++)
	{
		if (*(keysFound+i) == sData)
			std::cout << *(keysFound+i);//(t.search(Data(1,1)))->keys;
	}
	}
	*/

	// Seed the randomization
	srand(time(NULL));
	
	for (int i = 0; i < 1000; i++)
	{
		createRand(&t);
	}
	for (int i = 0; i < 10; i++)
	{
		findRand(&t);	
	}
	//t.traverse();
	
}
