/*************************************
 * Filename: Section01_ArrayInt.cc 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu
 * 
 * This file creates an Array<int> class
 * **********************************/

#include "Section01_DynArray.h"

const int LENGTH = 100;
const int TEST = 5;

/****************
 * Function Name: main
 * Preconditions: int, char**
 * Postconditions: int
 * This is the main driver Function
 * ***************/
int main(int argc, char **argv)
{
    srand(time(NULL));
 
    // Initialize the templates int array pointers   
    Array<int> *x = new Array<int>(LENGTH);
    Array<int> *y = x;
   
   // Generate a random number for all 
    for (int i = 0; i < LENGTH; i++) {
    	(*x)[i] = rand() % 1000;
    }
   
   std::cout << "The " << TEST << "th value of Array<int> x is " << (*x)[TEST] << std::endl;
   std::cout << "The " << TEST << "th value of Array<int> y is " << (*y)[TEST] << std::endl;
   
   delete x;
   // delete y; Not necessary, since y is pointing to x
   return 0;
}